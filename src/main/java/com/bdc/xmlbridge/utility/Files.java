package com.bdc.xmlbridge.utility;

import com.bdc.xmlbridge.model.file.IssuingXmlFile;
import com.bdc.xmlbridge.model.request.Request;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Component
public class Files {


    private static String pathName;
    private static int contractFilesCounter;
    private static int cardFilesCounter;
    private static int supplementaryFilesCounter;
    private static int modifyContractFilesCounter;
    private static int modifyCustomerDetailsCounter;

    static {
        contractFilesCounter = cardFilesCounter = supplementaryFilesCounter = modifyContractFilesCounter = modifyCustomerDetailsCounter = new Random().nextInt(1000);
    }

    public static XSSFWorkbook loadLookups() throws IOException {

        //creating a new file instance
        File file = new File(pathName);
        //obtaining bytes from the file
        FileInputStream fis = new FileInputStream(file);
        //creating Workbook instance that refers to .xlsx file
        return new XSSFWorkbook(fis);
    }

    public static String generateFile(Request request) {
        String type;
        IssuingXmlFile issuingXmlFile;
        int filesCounter;
        FileOutputStream fileOutputStream;
        issuingXmlFile = new IssuingXmlFile(request);
        String filed = "";
        switch (request.getClass().getSimpleName()) {
            case "IssuingContractIFileRequest":
                filed = "Cont";
                type = "Contract";
                filesCounter = contractFilesCounter;
                break;
            case "IssuingCardFileRequest":
                filed = "Card";
                type = "Card";
                filesCounter = cardFilesCounter;
                break;
            case "IssuingSupplementaryFileRequest":
                filed = "Card";
                type = "Supplementary";
                filesCounter = supplementaryFilesCounter;
                break;
            case "ModifyAccountCreditLimitFileRequest":
                type = "ModifyAccountCreditLimit";
                filesCounter = modifyContractFilesCounter;
                break;
            case "ModifyCustomerDetailsFileRequest":
                type = "ModifyCustomerDetailsFileRequest";
                filesCounter = modifyCustomerDetailsCounter;
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + request.getClass());
        }
        JAXBContext contextObj;
        try {
            contextObj = JAXBContext.newInstance(IssuingXmlFile.class);
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC003 : Failed to create " + type + " file\nFailed to find entity class\nPlease ask development team to check ContractFile class";
        }
        Marshaller marshallerObj;
        try {
            marshallerObj = contextObj.createMarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
            return "BDC003 :Failed to create " + type + " file\nFailed to create xml object\nPlease ask development team to check marshaller Object Property";
        }
        try {
            if (!filed.equals(""))
                marshallerObj.setProperty("com.sun.xml.internal.bind.xmlHeaders", "<?xml version='1.0' encoding='UTF-8'?>" + "<!--fileid=\"" + filed + filesCounter + "\"-->");
        } catch (PropertyException e) {
            e.printStackTrace();
            return "BDC003 : Failed to create " + type + " file\nFailed to create xml object\nPlease ask development team to check marshaller Object property";
        }
        try {
            marshallerObj.setProperty(Marshaller.JAXB_FRAGMENT, true);
        } catch (PropertyException e) {
            e.printStackTrace();
            return "BDC003 : Failed to create " + type + " file\nFailed to create xml object\nPlease ask development team to check marshaller Object Property";
        }
        try {
            marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (PropertyException e) {
            e.printStackTrace();
            return "BDC003 : Failed to create " + type + " file\nFailed to create xml object\nPlease ask development team to check  marshaller Object Property";
        }

        String fileName = fileNaming(request.getCardProductPrefix(), type);

        try {
            fileOutputStream = new FileOutputStream("Files/" + fileName + ".xml");
            marshallerObj.marshal(issuingXmlFile, fileOutputStream);
            fileOutputStream.close();
            fileCounter(type);
            return "BDC000 : " + type + " file  " + fileName + ".xml created successfully";
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC002: Failed to create :" + type + " file\nPlease ask development team to check Destination folder";
        }


    }

    private static String fileNaming(String cardProductPrefix, String type) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String cardDate = formatter.format(date);
        switch (type) {
            case "Contract": {
                return "BDC_" + cardProductPrefix + "_Open_Contract_" + contractFilesCounter + "_" + cardDate;
            }
            case "Card": {
                return "BDC_" + cardProductPrefix + "_Issue_Cards_Main_" + cardFilesCounter + "_" + cardDate;
            }
            case "Supplementary": {
                return "BDC_" + cardProductPrefix + "_Issue_Cards_Supp_" + supplementaryFilesCounter + "_" + cardDate;
            }
            case "ModifyAccountCreditLimit": {
                return "BDC_" + cardProductPrefix + "_Modify_Contract_" + modifyContractFilesCounter + "_" + cardDate;
            }
            case "ModifyCustomerDetails": {
                return "BDC_" + cardProductPrefix + "_Edit_Customer_Data_" + modifyCustomerDetailsCounter + "_" + cardDate;
            }

        }
        return cardDate;
    }

    private static void fileCounter(String type) {

        switch (type) {
            case "Contract": {
                contractFilesCounter++;
                break;
            }
            case "Card": {
                cardFilesCounter++;
                break;
            }
            case "Supplementary": {
                supplementaryFilesCounter++;
                break;
            }
            case "ModifyAccountCreditLimit": {
                modifyContractFilesCounter++;
                break;
            }
            case "ModifyCustomerDetails": {
                modifyCustomerDetailsCounter++;
                break;
            }

        }

    }

    @Value("${dictionary.dev.path}")
    private void setSvnUrl(String pathName) {
        Files.pathName = pathName;
    }

}









