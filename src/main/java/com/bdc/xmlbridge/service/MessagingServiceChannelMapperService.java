package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class MessagingServiceChannelMapperService {

    private static final String MESSAGING_SERVICE_CHANNEL_DICTIONARY = "Dict. of CMS Channels";
    private static final int MESSAGING_SERVICE_CHANNEL_INPUT_KEY = 1;
    private static final int MESSAGING_SERVICE_CHANNEL_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String messagingServiceSchemeMapper) {

        try {
            return lookups.mapLookups(MESSAGING_SERVICE_CHANNEL_DICTIONARY,MESSAGING_SERVICE_CHANNEL_INPUT_KEY, MESSAGING_SERVICE_CHANNEL_DESTINATION_KEY, messagingServiceSchemeMapper);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

