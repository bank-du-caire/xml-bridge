
package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class MessagingServiceSchemeMapperService {

    private static final String MESSAGING_SERVICE_SCHEME_DICTIONARY = "Dict. of CMS Schemes";
    private static final int MESSAGING_SERVICE_SCHEME_INPUT_KEY = 1;
    private static final int MESSAGING_SERVICE_SCHEME_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String messagingServiceSchemeMapper) {

        try {
            return lookups.mapLookups(MESSAGING_SERVICE_SCHEME_DICTIONARY,MESSAGING_SERVICE_SCHEME_INPUT_KEY, MESSAGING_SERVICE_SCHEME_DESTINATION_KEY, messagingServiceSchemeMapper);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

