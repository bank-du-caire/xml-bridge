package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class EducationMapperService {
    private static final String Education_DICTIONARY = "Dictionary of Education";
    private static final int Education_INPUT_KEY = 1;
    private static final int Education_DESTINATION_KEY = 0;

    public static String mapping(LookupsMapperService lookups, String education) {

        try {
            return lookups.mapLookups(Education_DICTIONARY, Education_INPUT_KEY,Education_DESTINATION_KEY, education);
        } catch (ValidationException ex) {
            return null;
        }
    }
}
