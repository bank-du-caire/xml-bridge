package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class OccupationMapperService {
    private static final String OCCUPATION_DICTIONARY = "Dict. of Occupation";
    private static final int OCCUPATION_INPUT_KEY = 1;
    private static final int OCCUPATION_DESTINATION_KEY = 0;

    public static String mapping(LookupsMapperService lookups, String occupation) {

        try {
            return lookups.mapLookups(OCCUPATION_DICTIONARY, OCCUPATION_INPUT_KEY, OCCUPATION_DESTINATION_KEY, occupation);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

