package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class LimitGroupMapperService {

    private static final String CARD_PRODUCT_PREFIX_DICTIONARY = "Dict. of Card Products";
    private static final int CARD_PRODUCT_PREFIX_INPUT_KEY = 1;
    private static final int CARD_PRODUCT_PREFIX_DESTINATION_KEY = 5;
    private static final String CARD_FINANCIAL_PROFILE_DICTIONARY = "Dict. of Card Fin. Prof.";
    private static final int CARD_FINANCIAL_PROFILE_INPUT_KEY = 1;
    private static final int CARD_FINANCIAL_PROFILE_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String cardPrefixInput) {
        String cardPrefix;
        try {
            cardPrefix = lookups.mapLookups(CARD_PRODUCT_PREFIX_DICTIONARY, CARD_PRODUCT_PREFIX_INPUT_KEY,CARD_PRODUCT_PREFIX_DESTINATION_KEY, cardPrefixInput);

            return lookups.mapLookups(CARD_FINANCIAL_PROFILE_DICTIONARY, CARD_FINANCIAL_PROFILE_INPUT_KEY,CARD_FINANCIAL_PROFILE_DESTINATION_KEY, cardPrefix);
        } catch (ValidationException ex) {
            return null;
        }
    }
}
