package com.bdc.xmlbridge.service;


import javax.xml.bind.ValidationException;


public class CardProductPrefixService {

    private static final String CARD_PRODUCT_PREFIX_DICTIONARY = "Dict. of Card Products";
    private static final int CARD_PRODUCT_PREFIX_INPUT_KEY = 1;
    private static final int CARD_PRODUCT_PREFIX_DESTINATION_KEY = 2;


    public static String mapping(LookupsMapperService lookups, String cardProductPrefix) {

        try {
            return lookups.mapLookups(CARD_PRODUCT_PREFIX_DICTIONARY, CARD_PRODUCT_PREFIX_INPUT_KEY,CARD_PRODUCT_PREFIX_DESTINATION_KEY, cardProductPrefix);
        } catch (
                ValidationException ex) {
            return null;
        }
    }
}
