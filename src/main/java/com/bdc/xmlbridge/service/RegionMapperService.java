package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class RegionMapperService {
    private static final String REGION_DICTIONARY = "Dict. of Regions";
    private static final int REGION_INPUT_KEY = 1;
    private static final int REGION_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String regionOfResidence) {

        try {
            return lookups.mapLookups(REGION_DICTIONARY, REGION_INPUT_KEY ,REGION_DESTINATION_KEY, regionOfResidence);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

