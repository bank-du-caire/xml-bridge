package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class CountryMapperService {
    private static final String COUNTRY_DICTIONARY = "Dict. of Countries";
    private static final int COUNTRY_INPUT_KEY = 3;
    private static final int COUNTRY_DESTINATION_KEY = 0;

    public static String mapping(LookupsMapperService lookups, String countryOfResidence) {

        try {
            return lookups.mapLookups(COUNTRY_DICTIONARY, COUNTRY_INPUT_KEY,COUNTRY_DESTINATION_KEY, countryOfResidence);
        } catch (ValidationException ex) {
            return null;
        }
    }
}
