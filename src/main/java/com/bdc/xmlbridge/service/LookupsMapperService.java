package com.bdc.xmlbridge.service;

import com.bdc.xmlbridge.utility.Files;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.util.Iterator;

@Component
public class LookupsMapperService {

    private XSSFWorkbook lookups;

    public void loadLookups() throws IOException {
        lookups = Files.loadLookups();
    }

    public String mapLookups(String dictionary, int inputKey, int destinationKey, String value) throws ValidationException {
        try {
            //creating a Sheet object to retrieve object
            XSSFSheet sheet = lookups.getSheet(dictionary);
            //iterating over excel file
            for (Row row : sheet) {
                //skip headlines row
                if (row.getRowNum() == 0) {
                    continue;
                }
                //iterating over each column
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    if (cell.getColumnIndex() == inputKey && cell.getStringCellValue().compareTo(value) == 0) {

                        switch (row.getCell(destinationKey).getCellType()) {
                            case 1:
                                return String.valueOf(row.getCell(destinationKey));
                            case 0:
                                double tmp = Double.parseDouble(String.valueOf(row.getCell(destinationKey)));
                                return String.valueOf((int) tmp);
                            default:
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        throw new ValidationException("this value not exist");
    }
}
