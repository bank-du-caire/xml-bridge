package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class MessagingServiceFinancialProfileMapperService {

    private static final String SMS_FIN_PROF_DICTIONARY = "Dict. of SMS Fin. Prof.";
    private static final int SMS_FIN_PROF_INPUT_KEY = 1;
    private static final int SMS_FIN_PROF_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String messagingServiceFinancialProfile) {

        try {
            return lookups.mapLookups(SMS_FIN_PROF_DICTIONARY, SMS_FIN_PROF_INPUT_KEY, SMS_FIN_PROF_DESTINATION_KEY, messagingServiceFinancialProfile);
        } catch (ValidationException ex) {
            return null;
        }
    }
}


