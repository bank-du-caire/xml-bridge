package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class MaritalStatusMapperService {

    private static final String MARITAL_STATUS_DICTIONARY = "Dict. of Marital Status";
    private static final int MARITAL_STATUS_INPUT_KEY = 1;
    private static final int MARITAL_STATUS_DESTINATION_KEY = 0;

    public static String mapping(LookupsMapperService lookups, String maritalStatus) {

        try {
            return lookups.mapLookups(MARITAL_STATUS_DICTIONARY,MARITAL_STATUS_INPUT_KEY, MARITAL_STATUS_DESTINATION_KEY, maritalStatus);
        } catch (ValidationException ex) {
            return null;
        }
    }

}
