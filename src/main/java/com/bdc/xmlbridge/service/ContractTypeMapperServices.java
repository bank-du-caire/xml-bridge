package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class ContractTypeMapperServices {
    private static final String CONTRACT_TYPE_DICTIONARY = "Dict. of Contract Types";
    private static final int CONTRACT_TYPE_INPUT_KEY = 1;
    private static final int CONTRACT_TYPE_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String cardProductPrefix) {

        try {
            return lookups.mapLookups(CONTRACT_TYPE_DICTIONARY, CONTRACT_TYPE_INPUT_KEY,CONTRACT_TYPE_DESTINATION_KEY, cardProductPrefix);
        } catch (ValidationException ex) {
            return null;
        }
    }
}
