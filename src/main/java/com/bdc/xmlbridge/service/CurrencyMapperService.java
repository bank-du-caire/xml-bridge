package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class CurrencyMapperService {

    private static final String CURRENCY_DICTIONARY = "Dict. of Currencies";
    private static final int CURRENCY_INPUT_KEY = 2;
    private static final int CURRENCY_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String currency) {

        try {
            return lookups.mapLookups(CURRENCY_DICTIONARY,CURRENCY_INPUT_KEY , CURRENCY_DESTINATION_KEY, currency);
        } catch (ValidationException ex) {
            return null;
        }
    }
}
