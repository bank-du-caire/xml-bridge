package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class CityMapperService {

    private static final String CITY_DICTIONARY = "Dict. of Cities";
    private static final int CITY_INPUT_KEY = 1;
    private static final int CITY_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String cityOfResidence) {

        try {
            return lookups.mapLookups(CITY_DICTIONARY, CITY_INPUT_KEY, CITY_DESTINATION_KEY, cityOfResidence);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

