package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class DirectDebitProfileMapperService {

    private static final String DIRECT_DEBIT_PROFILE_DICTIONARY = "Dict. of Direct Debit Profile";
    private static final int DIRECT_DEBIT_PROFILE_INPUT_KEY = 1;
    private static final int DIRECT_DEBIT_PROFILE_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String directDebitProfile) {

        try {
            return lookups.mapLookups(DIRECT_DEBIT_PROFILE_DICTIONARY,DIRECT_DEBIT_PROFILE_INPUT_KEY,DIRECT_DEBIT_PROFILE_DESTINATION_KEY, directDebitProfile);
        } catch (ValidationException ex) {
            return null;
        }
    }
}
