package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class TitleMapperService {
    private static final String TITLE_DICTIONARY = "Dictionary of Titles";
    private static final int TITLE_INPUT_KEY = 1;
    private static final int TITLE_DESTINATION_KEY = 0;

    public static String mapping(LookupsMapperService lookups, String title) {

        try {
            return lookups.mapLookups(TITLE_DICTIONARY,TITLE_INPUT_KEY , TITLE_DESTINATION_KEY, title);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

