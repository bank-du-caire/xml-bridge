package com.bdc.xmlbridge.service;

import com.bdc.xmlbridge.model.request.IIssuingContractIFileRequest;


public class AddressMapperService {

    public static void mapping(LookupsMapperService lookups, IIssuingContractIFileRequest request) {


        request.setRegionOfResidence(RegionMapperService.mapping(lookups, request.getRegionOfResidence()));
        request.setCountryOfResidence(CountryMapperService.mapping(lookups, request.getCountryOfResidence()));
        request.setCityOfResidence(CityMapperService.mapping(lookups, request.getCityOfResidence()));
        request.setRegionOfContact(RegionMapperService.mapping(lookups, request.getRegionOfContact()));
        request.setCountryOfContact(CountryMapperService.mapping(lookups, request.getCountryOfContact()));
        request.setCityOfContact(CityMapperService.mapping(lookups, request.getCityOfContact()));
    }
}


