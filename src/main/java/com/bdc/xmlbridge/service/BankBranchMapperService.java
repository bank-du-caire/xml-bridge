package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class BankBranchMapperService {

    private static final String BANK_BRANCH_DICTIONARY = "Dict. of Branches";
    private static final int BANK_BRANCH_INPUT_KEY = 1;
    private static final int BANK_BRANCH_DESTINATION_KEY = 0;


    public static String mapping(LookupsMapperService lookups, String bankBranch) {

        try {
            return lookups.mapLookups(BANK_BRANCH_DICTIONARY, BANK_BRANCH_INPUT_KEY,BANK_BRANCH_DESTINATION_KEY, bankBranch);
        } catch (ValidationException ex) {
            return null;
        }
    }
}

