
package com.bdc.xmlbridge.service;

import javax.xml.bind.ValidationException;

public class MobileNumberMapperService {
    private static final String PHONE_DICTIONARY = "Dict. of Countries";
    private static final int PHONE_INPUT_KEY = 3;
    private static final int PHONE_DESTINATION_KEY = 5;

    public static String mapping(LookupsMapperService lookups, String country, String inputPhone) {
        int inputPhoneLength = inputPhone.length();
        if (country.equals("EGYPT") && inputPhone.isEmpty()){
            String phone = "002";
            return phone + "(" + inputPhone.substring(1, 3) + ")" + inputPhone.substring(3, inputPhoneLength);

        } else{
            try {
                return lookups.mapLookups(PHONE_DICTIONARY, PHONE_INPUT_KEY, PHONE_DESTINATION_KEY, country) +
                        "(" + inputPhone.substring(1, 3) + ")" + inputPhone.substring(3, inputPhoneLength);
            } catch (ValidationException ex) {
                return null;
            }
        }
    }
}
