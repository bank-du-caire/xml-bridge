package com.bdc.xmlbridge.model.request;

public interface IIssuingFileRequest extends IRequest{


    void setCardProductPrefix(String cardProductPrefix);

    void setGender(String gender);

    void setName(String name);

    String getName();

    String getGender();

    String getCardProductPrefix();
}

