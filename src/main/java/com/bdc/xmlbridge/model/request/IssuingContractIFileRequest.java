package com.bdc.xmlbridge.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class IssuingContractIFileRequest extends IssuingFileRequest implements IIssuingContractIFileRequest {


    private String contractType;
    private String title;
    private String residencyIndication;
    private String residenceAddress;
    private String zipCodeOfResidence;
    private String countryOfResidence;
    private String regionOfResidence;
    private String cityOfResidence;
    private String contactAddress;
    private String zipCodeOfContact;
    private String countryOfContact;
    private String regionOfContact;
    private String cityOfContact;
    private String mobileNumber;
    private Date startJob;
    private int salary;
    private String maritalStatus;
    private String education;
    private String occupation;
    private String bankBranch;
    private Date startBank;
    private long limitInNationalCurrency;
    private long limitInInternationalCurrency;
    private String directDebitCalculationProfile;
    private Date birthday;
    private String homePhone;
    private String schemeParameters;

}