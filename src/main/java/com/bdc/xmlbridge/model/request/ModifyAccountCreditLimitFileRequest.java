package com.bdc.xmlbridge.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ModifyAccountCreditLimitFileRequest extends ModifyFileRequest {

    private String schemeParameters;
    private long limitInNationalCurrency;
    private long limitInInternationalCurrency;
}