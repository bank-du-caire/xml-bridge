package com.bdc.xmlbridge.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ModifyFileRequest extends Request {

    private String name;
    private String nationalID;

}
