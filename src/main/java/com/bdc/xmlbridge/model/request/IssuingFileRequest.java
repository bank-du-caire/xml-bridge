package com.bdc.xmlbridge.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;

@Getter
@Setter
@ToString
public class IssuingFileRequest extends Request {

    private String nationalID;
    @Nullable
    private String bankIdNumber;

    private String bankAccountNumber;

    private String name;
    private String gender;

}
