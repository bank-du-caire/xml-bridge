package com.bdc.xmlbridge.model.request;


public interface IIssuingCardIFileRequest extends IIssuingFileRequest {

    String getCardFinancialProfile();

    void setCardFinancialProfile(String cardFinancialProfile);

    String getLimitGroup();

    void setLimitGroup(String limitGroup);

    String getCurrency();

    void setCurrency(String currency);

    String getNameOnCard();

    void setNameOnCard(String nameOnCard);

    String getMessagingServiceFinancialProfile();

    void setMessagingServiceFinancialProfile(String messagingServiceFinancialProfile);

    String getMessagingServiceScheme();

    void setMessagingServiceScheme(String messagingServiceScheme);

    String getMessagingServiceChannel();


    void setMessagingServiceChannel(String messagingServiceChannel);

    String getMessagingServiceAddress();

    String getMessagingServiceActivityIndicator();

}
