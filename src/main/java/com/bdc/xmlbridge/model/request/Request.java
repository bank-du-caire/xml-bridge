package com.bdc.xmlbridge.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Request {

    private String contractNumber;
    private String cardProductPrefix;


}
