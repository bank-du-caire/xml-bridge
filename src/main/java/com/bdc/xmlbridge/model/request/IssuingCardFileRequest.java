package com.bdc.xmlbridge.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IssuingCardFileRequest extends IssuingFileRequest implements IIssuingCardIFileRequest {

    private String cardFinancialProfile;
    private String limitGroup;
    private String currency;
    private String nameOnCard;
    private String messagingServiceFinancialProfile;
    private String messagingServiceScheme;
    private String messagingServiceChannel;
    private String messagingServiceAddress;
    private String messagingServiceActivityIndicator;

}