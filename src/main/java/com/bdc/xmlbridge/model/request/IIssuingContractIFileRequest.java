package com.bdc.xmlbridge.model.request;

import java.util.Date;

public interface IIssuingContractIFileRequest extends IIssuingFileRequest {

    String getContractType();

    void setContractType(String contractType);

    String getTitle();

    void setTitle(String title);

    String getResidencyIndication();


    String getResidenceAddress();

    void setResidenceAddress(String residenceAddress);

    String getZipCodeOfResidence();


    String getCountryOfResidence();

    void setCountryOfResidence(String countryOfResidence);

    String getRegionOfResidence();

    void setRegionOfResidence(String regionOfResidence);

    String getCityOfResidence();

    void setCityOfResidence(String cityOfResidence);

    String getContactAddress();

    String getZipCodeOfContact();

    String getCountryOfContact();

    void setCountryOfContact(String countryOfContact);

    String getRegionOfContact();

    void setRegionOfContact(String regionOfContact);

    String getCityOfContact();

    void setCityOfContact(String cityOfContact);

    String getMobileNumber();

    void setMobileNumber(String mobileNumber);

    Date getStartJob();

    int getSalary();

    void setSalary(int salary);

    String getMaritalStatus();

    void setMaritalStatus(String maritalStatus);

    String getEducation();

    void setEducation(String education);

    String getOccupation();

    void setOccupation(String occupation);

    String getBankBranch();

    void setBankBranch(String bankBranch);

    Date getStartBank();

    long getLimitInNationalCurrency();

    long getLimitInInternationalCurrency();

    String getDirectDebitCalculationProfile();

    void setDirectDebitCalculationProfile(String directDebitCalculationProfile);

    Date getBirthday();

    String getHomePhone();

    void setHomePhone(String homePhone);

    String getSchemeParameters();

    void setSchemeParameters(String schemeParameters);
}
