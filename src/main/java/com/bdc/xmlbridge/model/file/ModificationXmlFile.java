package com.bdc.xmlbridge.model.file;


import com.bdc.xmlbridge.model.record.ModifyAccountCreditLimitFileRecord;
import com.bdc.xmlbridge.model.request.ModifyAccountCreditLimitFileRequest;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Root")
@NoArgsConstructor
public class ModificationXmlFile {


    @XmlElement(name = "Record")
    private ModifyAccountCreditLimitFileRecord modifyAccountCreditLimitFileRecord;

    public ModificationXmlFile(ModifyAccountCreditLimitFileRequest modifyAccountCreditLimitFileRequest) {

        modifyAccountCreditLimitFileRecord = new ModifyAccountCreditLimitFileRecord(modifyAccountCreditLimitFileRequest);
    }
}
