package com.bdc.xmlbridge.model.file;


import com.bdc.xmlbridge.model.record.IssuingCardFileRecord;
import com.bdc.xmlbridge.model.record.IssuingContractFileRecord;
import com.bdc.xmlbridge.model.record.IssuingSupplementaryFileRecord;
import com.bdc.xmlbridge.model.record.ModifyAccountCreditLimitFileRecord;
import com.bdc.xmlbridge.model.request.*;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Root")
@NoArgsConstructor
public class IssuingXmlFile {


    @XmlElement(name = "Record")
    private IssuingCardFileRecord issuingCardFileRecord;

    @XmlElement(name = "Record")
    private IssuingContractFileRecord issuingContractFileRecord;

    @XmlElement(name = "Record")
    private IssuingSupplementaryFileRecord issuingSupplementaryFileRecord;

    @XmlElement(name = "Record")
    private ModifyAccountCreditLimitFileRecord modifyAccountCreditLimitFileRecord;


    public IssuingXmlFile(Request request) {

        switch (request.getClass().getSimpleName()) {
            case "IssuingContractIFileRequest":
                issuingContractFileRecord = new IssuingContractFileRecord((IssuingContractIFileRequest) request);
                break;
            case "IssuingCardFileRequest":
                issuingCardFileRecord = new IssuingCardFileRecord((IssuingCardFileRequest) request);
                break;
            case "IssuingSupplementaryFileRequest":
                issuingSupplementaryFileRecord = new IssuingSupplementaryFileRecord((IssuingSupplementaryFileRequest) request);
                break;
            case "ModifyAccountCreditLimitFileRequest":
                modifyAccountCreditLimitFileRecord = new ModifyAccountCreditLimitFileRecord((ModifyAccountCreditLimitFileRequest) request);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + request.getClass());
        }


    }
}
