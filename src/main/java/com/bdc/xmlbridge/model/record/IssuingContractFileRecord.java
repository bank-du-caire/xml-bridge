package com.bdc.xmlbridge.model.record;

import com.bdc.xmlbridge.model.request.IssuingContractIFileRequest;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.text.SimpleDateFormat;

@XmlType(propOrder = {
        "TITLE",
        "RESIDENT",
        "ADRESS",
        "ZIPLIVE",
        "CNTRYLIVE",
        "REGIONLIVE",
        "CITYLIVE",
        "CORADDRESS",
        "ZIPCONT",
        "CNTRYCONT",
        "REGIONCONT",
        "CITYCONT",
        "CELLPHONE",
        "SALARY",
        "FAMILY",
        "EDUCATION",
        "OCCUPATION",
        "BRPART",
        "STARTBANK",
        "CONTYPE",
        "SCHPARAM",
        "BIRTHDAY",
        "PHONE",
        "STARTJOB",

})
@XmlRootElement(name = "Record")
@NoArgsConstructor
@Setter
@ToString
public class IssuingContractFileRecord extends IssuingFileRecord implements IIssuingContractFileRecord {


    private String contractTyp;
    private String schemeParameters;
    private String title;
    @Nullable
    private String resident;
    @Nullable
    private String address;
    @Nullable
    private String zipLive;
    @Nullable
    private String countryLive;
    @Nullable
    private String regionLive;
    @Nullable
    private String cityLive;
    @Nullable
    private String zipContact;
    @Nullable
    private String countryContact;
    @Nullable
    private String regionContact;
    @Nullable
    private String cityContact;
    @Nullable
    private String contactAddress;
    @Nullable
    private String celePhone;
    @Nullable
    private int salary;
    @Nullable
    private String maritalStatus;
    @Nullable
    private String education;
    @Nullable
    private String occupation;
    @Nullable
    private String bankBranch;
    @Nullable
    private String startBank;
    @Nullable
    private String birthday;
    @Nullable
    private String phone;
    @Nullable
    private String startJob;


    public IssuingContractFileRecord(IssuingContractIFileRequest contractFileRequest) {
        super(contractFileRequest.getContractNumber(),
                contractFileRequest.getName(),
                contractFileRequest.getGender(),
                contractFileRequest.getNationalID(),
                contractFileRequest.getCardProductPrefix(),
                contractFileRequest.getBankIdNumber(),
                contractFileRequest.getBankAccountNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        this.contractTyp = contractFileRequest.getContractType();
        this.schemeParameters = contractFileRequest.getSchemeParameters();
        this.birthday = formatter.format(contractFileRequest.getBirthday());
        this.phone = contractFileRequest.getMobileNumber();
        this.startJob = formatter.format(contractFileRequest.getStartJob());
        this.resident = contractFileRequest.getResidencyIndication();
        this.address = contractFileRequest.getResidenceAddress();
        this.zipLive = contractFileRequest.getZipCodeOfResidence();
        this.countryLive = contractFileRequest.getCountryOfResidence();
        this.regionLive = contractFileRequest.getRegionOfResidence();
        this.cityLive = contractFileRequest.getCityOfResidence();
        this.zipContact = contractFileRequest.getZipCodeOfContact();
        this.countryContact = contractFileRequest.getCountryOfContact();
        this.regionContact = contractFileRequest.getRegionOfContact();
        this.cityContact = contractFileRequest.getCityOfContact();
        this.contactAddress = contractFileRequest.getContactAddress();
        this.celePhone = contractFileRequest.getMobileNumber();
        this.salary = 12 * contractFileRequest.getSalary();
        this.maritalStatus = contractFileRequest.getMaritalStatus();
        this.education = contractFileRequest.getEducation();
        this.occupation = contractFileRequest.getOccupation();
        this.bankBranch = contractFileRequest.getBankBranch();
        this.startBank = formatter.format(contractFileRequest.getStartBank());
        this.title = contractFileRequest.getTitle();


    }

    @XmlElement(name = "CONTYPE")
    public String getCONTYPE() {
        return contractTyp;
    }

    @XmlElement(name = "SCHPARAM")
    public String getSCHPARAM() {
        return schemeParameters;
    }

    @Nullable
    @XmlElement(name = "BIRTHDAY")
    public String getBIRTHDAY() {
        return birthday;
    }

    @Nullable
    @XmlElement(name = "PHONE")
    public String getPHONE() {
        return phone;
    }

    @Nullable
    @XmlElement(name = "STARTJOB")
    public String getSTARTJOB() {
        return startJob;
    }

    @Nullable
    @XmlElement(name = "RESIDENT")
    public String getRESIDENT() {
        return resident;
    }

    @Nullable
    @XmlElement(name = "ADRESS")
    public String getADRESS() {
        return address;
    }

    @Nullable
    @XmlElement(name = "ZIPLIVE")
    public String getZIPLIVE() {
        return zipLive;
    }

    @Nullable
    @XmlElement(name = "CNTRYLIVE")
    public String getCNTRYLIVE() {
        return countryLive;
    }

    @Nullable
    @XmlElement(name = "REGIONLIVE")
    public String getREGIONLIVE() {
        return regionLive;
    }

    @Nullable
    @XmlElement(name = "CITYLIVE")
    public String getCITYLIVE() {
        return cityLive;
    }

    @Nullable
    @XmlElement(name = "ZIPCONT")
    public String getZIPCONT() {
        return zipContact;
    }

    @Nullable
    @XmlElement(name = "CNTRYCONT")
    public String getCNTRYCONT() {
        return countryContact;
    }

    @Nullable
    @XmlElement(name = "REGIONCONT")
    public String getREGIONCONT() {
        return regionContact;
    }

    @Nullable
    @XmlElement(name = "CITYCONT")
    public String getCITYCONT() {
        return cityContact;
    }

    @Nullable
    @XmlElement(name = "CORADDRESS")
    public String getCORADDRESS() {
        return contactAddress;
    }

    @Nullable
    @XmlElement(name = "CELLPHONE")
    public String getCELLPHONE() {
        return celePhone;
    }

    @Nullable
    @XmlElement(name = "SALARY")
    public int getSALARY() {
        return salary;
    }

    @Nullable
    @XmlElement(name = "FAMILY")
    public String getFAMILY() {
        return maritalStatus;
    }

    @Nullable
    @XmlElement(name = "EDUCATION")
    public String getEDUCATION() {
        return education;
    }

    @Nullable
    @XmlElement(name = "OCCUPATION")
    public String getOCCUPATION() {
        return occupation;
    }

    @Nullable
    @XmlElement(name = "BRPART")
    public String getBRPART() {
        return bankBranch;
    }

    @Nullable
    @XmlElement(name = "STARTBANK")
    public String getSTARTBANK() {
        return startBank;
    }

    @XmlElement(name = "TITLE")
    public String getTITLE() {
        return title;
    }

//    @Override
//    public void setName(String name) {
//
//    }
//    @Override
//    public void setNationalId(String nationalId) {
//
//    }
}
