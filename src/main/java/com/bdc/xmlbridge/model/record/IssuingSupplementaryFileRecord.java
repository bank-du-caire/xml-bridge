package com.bdc.xmlbridge.model.record;

import com.bdc.xmlbridge.model.request.IssuingSupplementaryFileRequest;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.text.SimpleDateFormat;

@XmlType(propOrder = {
        "TITLE",
        "RESIDENT",
        "ADRESS",
        "ZIPLIVE",
        "CNTRYLIVE",
        "REGIONLIVE",
        "CITYLIVE",
        "CORADDRESS",
        "ZIPCONT",
        "CNTRYCONT",
        "REGIONCONT",
        "CITYCONT",
        "CELLPHONE",
        "SALARY",
        "FAMILY",
        "EDUCATION",
        "OCCUPATION",
        "BRPART",
        "STARTBANK",
        "CONTYPE",
        "SCHPARAM",
        "BIRTHDAY",
        "PHONE",
        "STARTJOB",
        "FINPROF",
        "GROUPCMD",
        "CURRENCYNO",
        "NAMEONCARD",
        "CMSPROFILE",
        "CNSCARDS",
        "CNSCARDCH",
        "CNSCARDA",
        "CMSACTIVE",

})
@XmlRootElement(name = "Record")
@Setter
@ToString
@NoArgsConstructor
public class IssuingSupplementaryFileRecord extends IssuingFileRecord implements IIssuingContractFileRecord, IIssuingCardFileRecord {


    private String contractTyp;
    private String schemeParameters;
    private String title;
    @Nullable
    private String resident;
    @Nullable
    private String address;
    @Nullable
    private String zipLive;
    @Nullable
    private String countryLive;
    @Nullable
    private String regionLive;
    @Nullable
    private String cityLive;
    @Nullable
    private String zipContact;
    @Nullable
    private String countryContact;
    @Nullable
    private String regionContact;
    @Nullable
    private String cityContact;
    @Nullable
    private String contactAddress;
    @Nullable
    private String celePhone;
    @Nullable
    private int salary;
    @Nullable
    private String maritalStatus;
    @Nullable
    private String education;
    @Nullable
    private String occupation;
    @Nullable
    private String bankBranch;
    @Nullable
    private String startBank;
    @Nullable
    private String birthday;
    @Nullable
    private String phone;
    @Nullable
    private String startJob;
    private String financialProfile;
    private String usageLimitProfile;
    private String currencyNo;
    private String nameOnCard;
    private String messagingServiceProfile;
    private String codeOfTheMessagingServiceScheme;
    private String codeOfTheMessagingServiceChannel;
    private String messagingServiceAddress;
    private String messagingServiceActivityIndicator;


    public IssuingSupplementaryFileRecord(IssuingSupplementaryFileRequest supplementaryFileRequest) {
        super(supplementaryFileRequest.getContractNumber(),
                supplementaryFileRequest.getName(),
                supplementaryFileRequest.getGender(),
                supplementaryFileRequest.getNationalID(),
                supplementaryFileRequest.getCardProductPrefix(),
                supplementaryFileRequest.getBankIdNumber(),
                supplementaryFileRequest.getBankAccountNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        this.contractTyp = supplementaryFileRequest.getContractType();
        this.schemeParameters = supplementaryFileRequest.getSchemeParameters();
        this.birthday = formatter.format(supplementaryFileRequest.getBirthday());
        this.phone = supplementaryFileRequest.getMobileNumber();
        this.startJob = formatter.format(supplementaryFileRequest.getStartJob());
        this.resident = supplementaryFileRequest.getResidencyIndication();
        this.address = supplementaryFileRequest.getResidenceAddress();
        this.zipLive = supplementaryFileRequest.getZipCodeOfResidence();
        this.countryLive = supplementaryFileRequest.getCountryOfResidence();
        this.regionLive = supplementaryFileRequest.getRegionOfResidence();
        this.cityLive = supplementaryFileRequest.getCityOfResidence();
        this.zipContact = supplementaryFileRequest.getZipCodeOfContact();
        this.countryContact = supplementaryFileRequest.getCountryOfContact();
        this.regionContact = supplementaryFileRequest.getRegionOfContact();
        this.cityContact = supplementaryFileRequest.getCityOfContact();
        this.contactAddress = supplementaryFileRequest.getContactAddress();
        this.celePhone = supplementaryFileRequest.getMobileNumber();
        this.salary = 12 * supplementaryFileRequest.getSalary();
        this.maritalStatus = supplementaryFileRequest.getMaritalStatus();
        this.education = supplementaryFileRequest.getEducation();
        this.occupation = supplementaryFileRequest.getOccupation();
        this.bankBranch = supplementaryFileRequest.getBankBranch();
        this.startBank = formatter.format(supplementaryFileRequest.getStartBank());
        this.title = supplementaryFileRequest.getTitle();
        this.financialProfile = supplementaryFileRequest.getCardFinancialProfile();
        this.usageLimitProfile = supplementaryFileRequest.getLimitGroup();
        this.currencyNo = supplementaryFileRequest.getCurrency();
        this.nameOnCard = supplementaryFileRequest.getNameOnCard();
        this.messagingServiceProfile = supplementaryFileRequest.getMessagingServiceFinancialProfile();
        this.codeOfTheMessagingServiceScheme = supplementaryFileRequest.getMessagingServiceScheme();
        this.codeOfTheMessagingServiceChannel = supplementaryFileRequest.getMessagingServiceChannel();
        this.messagingServiceAddress = supplementaryFileRequest.getMessagingServiceAddress();
        this.messagingServiceActivityIndicator = supplementaryFileRequest.getMessagingServiceActivityIndicator();


    }

    @XmlElement(name = "CONTYPE")
    public String getCONTYPE() {
        return contractTyp;
    }

    @XmlElement(name = "SCHPARAM")
    public String getSCHPARAM() {
        return schemeParameters;
    }

    @Nullable
    @XmlElement(name = "BIRTHDAY")
    public String getBIRTHDAY() {
        return birthday;
    }

    @Nullable
    @XmlElement(name = "PHONE")
    public String getPHONE() {
        return phone;
    }

    @Nullable
    @XmlElement(name = "STARTJOB")
    public String getSTARTJOB() {
        return startJob;
    }

    @Nullable
    @XmlElement(name = "RESIDENT")
    public String getRESIDENT() {
        return resident;
    }

    @Nullable
    @XmlElement(name = "ADRESS")
    public String getADRESS() {
        return address;
    }

    @Nullable
    @XmlElement(name = "ZIPLIVE")
    public String getZIPLIVE() {
        return zipLive;
    }

    @Nullable
    @XmlElement(name = "CNTRYLIVE")
    public String getCNTRYLIVE() {
        return countryLive;
    }

    @Nullable
    @XmlElement(name = "REGIONLIVE")
    public String getREGIONLIVE() {
        return regionLive;
    }

    @Nullable
    @XmlElement(name = "CITYLIVE")
    public String getCITYLIVE() {
        return cityLive;
    }

    @Nullable
    @XmlElement(name = "ZIPCONT")
    public String getZIPCONT() {
        return zipContact;
    }

    @Nullable
    @XmlElement(name = "CNTRYCONT")
    public String getCNTRYCONT() {
        return countryContact;
    }

    @Nullable
    @XmlElement(name = "REGIONCONT")
    public String getREGIONCONT() {
        return regionContact;
    }

    @Nullable
    @XmlElement(name = "CITYCONT")
    public String getCITYCONT() {
        return cityContact;
    }

    @Nullable
    @XmlElement(name = "CORADDRESS")
    public String getCORADDRESS() {
        return contactAddress;
    }

    @Nullable
    @XmlElement(name = "CELLPHONE")
    public String getCELLPHONE() {
        return celePhone;
    }

    @Nullable
    @XmlElement(name = "SALARY")
    public int getSALARY() {
        return salary;
    }

    @Nullable
    @XmlElement(name = "FAMILY")
    public String getFAMILY() {
        return maritalStatus;
    }

    @Nullable
    @XmlElement(name = "EDUCATION")
    public String getEDUCATION() {
        return education;
    }

    @Nullable
    @XmlElement(name = "OCCUPATION")
    public String getOCCUPATION() {
        return occupation;
    }

    @Nullable
    @XmlElement(name = "BRPART")
    public String getBRPART() {
        return bankBranch;
    }

    @Nullable
    @XmlElement(name = "STARTBANK")
    public String getSTARTBANK() {
        return startBank;
    }


    @XmlElement(name = "TITLE")
    public String getTITLE() {
        return title;
    }

    @XmlElement(name = "FINPROF")
    public String getFINPROF() {
        return financialProfile;
    }

    @XmlElement(name = "GROUPCMD")
    public String getGROUPCMD() {
        return usageLimitProfile;
    }

    @XmlElement(name = "CURRENCYNO")
    public String getCURRENCYNO() {
        return currencyNo;
    }

    @XmlElement(name = "NAMEONCARD")
    public String getNAMEONCARD() {
        return nameOnCard;
    }

    @XmlElement(name = "CMSPROFILE")
    public String getCMSPROFILE() {
        return messagingServiceProfile;
    }

    @XmlElement(name = "CNSCARDS")
    public String getCNSCARDS() {
        return codeOfTheMessagingServiceScheme;
    }

    @XmlElement(name = "CNSCARDCH")
    public String getCNSCARDCH() {
        return codeOfTheMessagingServiceChannel;
    }

    @XmlElement(name = "CNSCARDA")
    public String getCNSCARDA() {
        return messagingServiceAddress;
    }

    @XmlElement(name = "CMSACTIVE")
    public String getCMSACTIVE() {
        return messagingServiceActivityIndicator;
    }

}
