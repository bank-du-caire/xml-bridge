package com.bdc.xmlbridge.model.record;

import org.springframework.lang.Nullable;

import javax.xml.bind.annotation.XmlElement;

public interface IIssuingFileRecord {
    @Nullable
    @XmlElement(name = "EXTID")
    String getEXTID();

    @XmlElement(name = "EXTACCOUNT")
    String getEXTACCOUNT();

    @XmlElement(name = "CONTRACTNO")
    String getCONTRACTNO();

    @XmlElement(name = "PASNOM")
    String getPASNOM();


    @XmlElement(name = "SEX")
    String getSEX();



    @XmlElement(name = "FIO")
    String getFIO();

    @XmlElement(name = "CARDPREFIX")
    String getCARDPREFIX();

    void setExternalId(@Nullable String externalId);



}
