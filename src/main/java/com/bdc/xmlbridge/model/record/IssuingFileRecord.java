package com.bdc.xmlbridge.model.record;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.lang.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder = {
        "CONTRACTNO",
        "CARDPREFIX",
        "FIO",
        "SEX",
        "PASNOM",
        "EXTID",
        "EXTACCOUNT"
})
@ToString
@NoArgsConstructor
public abstract class IssuingFileRecord {

    private String contractNo;
    private String name;
    private String gender;
    private String nationalId;
    private String cardPrefix;
    @Nullable
    private String externalId;
    private String externalAccount;


    public IssuingFileRecord(String contractNo, String name, String gender, String nationalId, String cardPrefix, @Nullable String externalId, String externalAccount) {
        this.contractNo = contractNo;
        this.name = name;
        this.gender = gender;
        this.nationalId = nationalId;
        this.cardPrefix = cardPrefix;
        this.externalId = externalId;
        this.externalAccount = externalAccount;
    }


    @Nullable
    @XmlElement(name = "EXTID")
    public String getEXTID() {
        return externalId;
    }

    @XmlElement(name = "EXTACCOUNT")
    public String getEXTACCOUNT() {
        return externalAccount;
    }

    @XmlElement(name = "CONTRACTNO")
    public String getCONTRACTNO() {
        return contractNo;
    }

    @XmlElement(name = "PASNOM")
    public String getPASNOM() {
        return nationalId;
    }


    @XmlElement(name = "SEX")
    public String getSEX() {
        return gender;
    }

    @XmlElement(name = "FIO")
    public String getFIO() {
        return name;
    }

    @XmlElement(name = "CARDPREFIX")
    public String getCARDPREFIX() {
        return cardPrefix;
    }

    public void setExternalId(@Nullable String externalId) {
        this.externalId = externalId;
    }

}
