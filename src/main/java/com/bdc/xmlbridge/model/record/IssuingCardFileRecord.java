package com.bdc.xmlbridge.model.record;

import com.bdc.xmlbridge.model.request.IssuingCardFileRequest;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Record")
@XmlType(propOrder = {
        "FINPROF",
        "GROUPCMD",
        "CURRENCYNO",
        "NAMEONCARD",
        "CMSPROFILE",
        "CNSCARDS",
        "CNSCARDCH",
        "CNSCARDA",
        "CMSACTIVE",
})
@ToString
@NoArgsConstructor
public class IssuingCardFileRecord extends IssuingFileRecord implements IIssuingCardFileRecord {


    private String financialProfile;
    private String usageLimitProfile;
    private String currencyNo;
    private String nameOnCard;
    private String messagingServiceProfile;
    private String codeOfTheMessagingServiceScheme;
    private String codeOfTheMessagingServiceChannel;
    private String messagingServiceAddress;
    private String messagingServiceActivityIndicator;


    public IssuingCardFileRecord(IssuingCardFileRequest cardFileRequest) {

        super(
                cardFileRequest.getContractNumber(),
                cardFileRequest.getName(),
                cardFileRequest.getGender(),
                cardFileRequest.getNationalID(),
                cardFileRequest.getCardProductPrefix(),
                cardFileRequest.getBankIdNumber(),
                cardFileRequest.getBankAccountNumber()

        );
        this.financialProfile = cardFileRequest.getCardFinancialProfile();
        this.usageLimitProfile = cardFileRequest.getLimitGroup();
        this.currencyNo = cardFileRequest.getCurrency();
        this.nameOnCard = cardFileRequest.getNameOnCard();
        this.messagingServiceProfile = cardFileRequest.getMessagingServiceFinancialProfile();
        this.codeOfTheMessagingServiceScheme = cardFileRequest.getMessagingServiceScheme();
        this.codeOfTheMessagingServiceChannel = cardFileRequest.getMessagingServiceChannel();
        this.messagingServiceAddress = cardFileRequest.getMessagingServiceAddress();
        this.messagingServiceActivityIndicator = cardFileRequest.getMessagingServiceActivityIndicator();


    }

    @XmlElement(name = "FINPROF")
    public String getFINPROF() {
        return financialProfile;
    }

    @XmlElement(name = "GROUPCMD")
    public String getGROUPCMD() {
        return usageLimitProfile;
    }

    @XmlElement(name = "CURRENCYNO")
    public String getCURRENCYNO() {
        return currencyNo;
    }

    @XmlElement(name = "NAMEONCARD")
    public String getNAMEONCARD() {
        return nameOnCard;
    }

    @XmlElement(name = "CMSPROFILE")
    public String getCMSPROFILE() {
        return messagingServiceProfile;
    }

    @XmlElement(name = "CNSCARDS")
    public String getCNSCARDS() {
        return codeOfTheMessagingServiceScheme;
    }

    @XmlElement(name = "CNSCARDCH")
    public String getCNSCARDCH() {
        return codeOfTheMessagingServiceChannel;
    }

    @XmlElement(name = "CNSCARDA")
    public String getCNSCARDA() {
        return messagingServiceAddress;
    }

    @XmlElement(name = "CMSACTIVE")
    public String getCMSACTIVE() {
        return messagingServiceActivityIndicator;
    }


}