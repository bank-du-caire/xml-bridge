package com.bdc.xmlbridge.model.record;

import com.bdc.xmlbridge.model.request.ModifyAccountCreditLimitFileRequest;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder = {
        "CONTRACTNO",
        "SCHPARAM",
        "FIO",
        "PASNOM",
})
@ToString
@NoArgsConstructor
public class ModifyAccountCreditLimitFileRecord {

    private String contractNo;
    private String name;
    private String nationalId;
    private String schemeParameters;

    public ModifyAccountCreditLimitFileRecord(ModifyAccountCreditLimitFileRequest modifyAccountCreditLimitFileRequest) {
        this.contractNo = modifyAccountCreditLimitFileRequest.getContractNumber();
        this.name = modifyAccountCreditLimitFileRequest.getName();
        this.nationalId = modifyAccountCreditLimitFileRequest.getNationalID();
        this.schemeParameters = modifyAccountCreditLimitFileRequest.getSchemeParameters();
    }

    @XmlElement(name = "SCHPARAM")
    public String getSCHPARAM() {
        return schemeParameters;
    }

    public void setSCHPARAM(String schemeParameters) {
        this.schemeParameters = schemeParameters;
    }

    @XmlElement(name = "CONTRACTNO")
    public String getCONTRACTNO() {
        return contractNo;
    }

    @XmlElement(name = "PASNOM")
    public String getPASNOM() {
        return nationalId;
    }

    @XmlElement(name = "FIO")
    public String getFIO() {
        return name;
    }

}
