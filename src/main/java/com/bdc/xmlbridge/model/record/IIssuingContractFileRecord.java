package com.bdc.xmlbridge.model.record;

import org.springframework.lang.Nullable;

import javax.xml.bind.annotation.XmlElement;

public interface IIssuingContractFileRecord extends IIssuingFileRecord {

    @XmlElement(name = "CONTYPE")
    String getCONTYPE();

    @XmlElement(name = "SCHPARAM")
    String getSCHPARAM();

    @Nullable
    @XmlElement(name = "BIRTHDAY")
    String getBIRTHDAY();

    @Nullable
    @XmlElement(name = "PHONE")
    String getPHONE();

    @Nullable
    @XmlElement(name = "STARTJOB")
    String getSTARTJOB();

    @Nullable
    @XmlElement(name = "RESIDENT")
    String getRESIDENT();

    @Nullable
    @XmlElement(name = "ADRESS")
    String getADRESS();

    @Nullable
    @XmlElement(name = "ZIPLIVE")
    String getZIPLIVE();

    @Nullable
    @XmlElement(name = "CNTRYLIVE")
    String getCNTRYLIVE();

    @Nullable
    @XmlElement(name = "REGIONLIVE")
    String getREGIONLIVE();

    @Nullable
    @XmlElement(name = "CITYLIVE")
    String getCITYLIVE();

    @Nullable
    @XmlElement(name = "ZIPCONT")
    String getZIPCONT();

    @Nullable
    @XmlElement(name = "CNTRYCONT")
    String getCNTRYCONT();

    @Nullable
    @XmlElement(name = "REGIONCONT")
    String getREGIONCONT();

    @Nullable
    @XmlElement(name = "CITYCONT")
    String getCITYCONT();

    @Nullable
    @XmlElement(name = "CORADDRESS")
    String getCORADDRESS();

    @Nullable
    @XmlElement(name = "CELLPHONE")
    String getCELLPHONE();

    @Nullable
    @XmlElement(name = "SALARY")
    int getSALARY();

    @Nullable
    @XmlElement(name = "FAMILY")
    String getFAMILY();

    @Nullable
    @XmlElement(name = "EDUCATION")
    String getEDUCATION();

    @Nullable
    @XmlElement(name = "OCCUPATION")
    String getOCCUPATION();

    @Nullable
    @XmlElement(name = "BRPART")
    String getBRPART();

    @Nullable
    @XmlElement(name = "STARTBANK")
    String getSTARTBANK();


    @XmlElement(name = "TITLE")
    String getTITLE();
}
