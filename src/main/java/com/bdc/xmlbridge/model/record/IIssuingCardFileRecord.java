package com.bdc.xmlbridge.model.record;

import javax.xml.bind.annotation.XmlElement;

public interface IIssuingCardFileRecord extends IIssuingFileRecord {
    @XmlElement(name = "FINPROF")
    String getFINPROF();

    @XmlElement(name = "GROUPCMD")
    String getGROUPCMD();

    @XmlElement(name = "CURRENCYNO")
    String getCURRENCYNO();

    @XmlElement(name = "NAMEONCARD")
    String getNAMEONCARD();

    @XmlElement(name = "CMSPROFILE")
    String getCMSPROFILE();

    @XmlElement(name = "CNSCARDS")
    String getCNSCARDS();

    @XmlElement(name = "CNSCARDCH")
    String getCNSCARDCH();

    @XmlElement(name = "CNSCARDA")
    String getCNSCARDA();

    @XmlElement(name = "CMSACTIVE")
    String getCMSACTIVE();


}
