package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.ModifyAccountCreditLimitFileRequest;
import com.bdc.xmlbridge.service.CardProductPrefixService;
import com.bdc.xmlbridge.service.LookupsMapperService;

public interface IModifyAccountCreditLimitMapper {
    default void ModifyAccountCreditLimitMapping(ModifyAccountCreditLimitFileRequest modifyAccountCreditLimitFileRequest, LookupsMapperService lookups) {

        modifyAccountCreditLimitFileRequest.setCardProductPrefix(CardProductPrefixService.mapping(lookups, modifyAccountCreditLimitFileRequest.getCardProductPrefix()));
        String schemeParameters = "CONTRACT_ID=" + modifyAccountCreditLimitFileRequest.getContractNumber();

        if (modifyAccountCreditLimitFileRequest.getLimitInNationalCurrency() != 0)
            schemeParameters += ";LIMIT1=" + modifyAccountCreditLimitFileRequest.getLimitInNationalCurrency();
        else
            schemeParameters += ";LIMIT2=" + modifyAccountCreditLimitFileRequest.getLimitInInternationalCurrency();

        modifyAccountCreditLimitFileRequest.setSchemeParameters(schemeParameters);

    }
}
