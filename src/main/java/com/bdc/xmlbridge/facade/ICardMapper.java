package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.IssuingCardFileRequest;
import com.bdc.xmlbridge.model.request.IIssuingCardIFileRequest;
import com.bdc.xmlbridge.model.request.IssuingFileRequest;
import com.bdc.xmlbridge.model.request.IssuingSupplementaryFileRequest;
import com.bdc.xmlbridge.service.*;

public interface ICardMapper {


    default void CardMapping(IssuingFileRequest issuingFileRequest, LookupsMapperService lookups) {

        IIssuingCardIFileRequest file;

        if (issuingFileRequest instanceof IssuingCardFileRequest) {
            file = ((IssuingCardFileRequest) issuingFileRequest);
        } else {
            file = (IssuingSupplementaryFileRequest) issuingFileRequest;
        }
        file.setCardFinancialProfile(CardFinancialProfileMapperService.mapping(lookups, file.getCardProductPrefix()));
        file.setLimitGroup(LimitGroupMapperService.mapping(lookups, file.getCardProductPrefix()));
        file.setCardProductPrefix(CardProductPrefixService.mapping(lookups, file.getCardProductPrefix()));
        file.setCurrency(CurrencyMapperService.mapping(lookups, file.getCurrency()));
        file.setMessagingServiceFinancialProfile(MessagingServiceFinancialProfileMapperService.mapping(lookups, file.getMessagingServiceFinancialProfile()));
        file.setMessagingServiceScheme(MessagingServiceSchemeMapperService.mapping(lookups, file.getMessagingServiceScheme()));
        file.setMessagingServiceChannel(MessagingServiceChannelMapperService.mapping(lookups, file.getMessagingServiceChannel()));
    }

}
