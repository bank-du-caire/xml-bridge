package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.IssuingContractIFileRequest;
import com.bdc.xmlbridge.service.LookupsMapperService;
import com.bdc.xmlbridge.utility.Files;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component
public class ContractMapper implements IContractMapper {
    @Resource
    private LookupsMapperService lookups;


    public String Mapping(IssuingContractIFileRequest contractFileRequest) {
        try {
            lookups.loadLookups();
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC001 :\nFailed to create contract file\nFailed to read lookups file\nPlease ask developer team to check looks dictionary path ";
        }
        this.ContractMapping(contractFileRequest, lookups);

        try {
            return Files.generateFile(contractFileRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC002 :\nFailed to create Contract file please check destination folder";
        }

    }
}