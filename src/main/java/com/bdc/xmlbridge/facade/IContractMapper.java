package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.IssuingContractIFileRequest;
import com.bdc.xmlbridge.model.request.IIssuingContractIFileRequest;
import com.bdc.xmlbridge.model.request.IssuingFileRequest;
import com.bdc.xmlbridge.model.request.IssuingSupplementaryFileRequest;
import com.bdc.xmlbridge.service.*;

public interface IContractMapper {

    default void ContractMapping(IssuingFileRequest issuingFileRequest, LookupsMapperService lookups) {

        IIssuingContractIFileRequest file;

        if (issuingFileRequest instanceof IssuingContractIFileRequest) {
            file = ((IssuingContractIFileRequest) issuingFileRequest);
            file.setCardProductPrefix(CardProductPrefixService.mapping(lookups, file.getCardProductPrefix()));
        } else {
            file = (IssuingSupplementaryFileRequest) issuingFileRequest;
        }
        file.setContractType(ContractTypeMapperServices.mapping(lookups, file.getContractType()));
        file.setTitle(TitleMapperService.mapping(lookups, file.getTitle()));
        file.setHomePhone(HomePhoneMapperService.mapping(lookups, file.getRegionOfResidence(), file.getHomePhone()));
        file.setMobileNumber(MobileNumberMapperService.mapping(lookups, file.getCountryOfResidence(), file.getMobileNumber()));
        AddressMapperService.mapping(lookups, file);
        file.setMaritalStatus(MaritalStatusMapperService.mapping(lookups, file.getMaritalStatus()));
        file.setEducation(EducationMapperService.mapping(lookups, file.getEducation()));
        file.setOccupation(OccupationMapperService.mapping(lookups, file.getOccupation()));
        file.setDirectDebitCalculationProfile(DirectDebitProfileMapperService.mapping(lookups, file.getDirectDebitCalculationProfile()));
        String schemeParameters = "LIMIT1=" + file.getLimitInNationalCurrency();
        if (file.getLimitInInternationalCurrency() != 0)
            schemeParameters += ";LIMIT2=" + file.getLimitInInternationalCurrency();
        schemeParameters += ";DAF1=ON@" + file.getDirectDebitCalculationProfile();
        file.setSchemeParameters(schemeParameters);
        file.setBankBranch(BankBranchMapperService.mapping(lookups, file.getBankBranch()));

    }

}