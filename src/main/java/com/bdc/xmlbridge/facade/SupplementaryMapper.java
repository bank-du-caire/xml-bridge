package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.IssuingSupplementaryFileRequest;
import com.bdc.xmlbridge.service.LookupsMapperService;
import com.bdc.xmlbridge.utility.Files;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component
public class SupplementaryMapper implements IContractMapper, ICardMapper {
    @Resource
    private LookupsMapperService lookups;


    public String Mapping(IssuingSupplementaryFileRequest supplementaryFileRequest) {
        try {
            lookups.loadLookups();
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC001 :\nFailed to create supplementary file\nFailed to read lookups file\nPlease ask developer team to check looks dictionary path ";
        }

        this.ContractMapping(supplementaryFileRequest, lookups);
        this.CardMapping(supplementaryFileRequest, lookups);

        try {
            return Files.generateFile(supplementaryFileRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC002 :\nFailed to create Contract file please check request parameters";
        }
    }


}

