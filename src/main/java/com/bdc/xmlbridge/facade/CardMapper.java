package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.IssuingCardFileRequest;
import com.bdc.xmlbridge.service.LookupsMapperService;
import com.bdc.xmlbridge.utility.Files;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class CardMapper implements ICardMapper {
    @Resource
    private LookupsMapperService lookups;


    public String Mapping(IssuingCardFileRequest cardFileRequest) {
        try {
            lookups.loadLookups();
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC001 :\nFailed to create card file\nFailed to read lookups file\nPlease ask developer team to check looks dictionary path ";
        }
        this.CardMapping(cardFileRequest, lookups);

        try {
            return Files.generateFile(cardFileRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC002 :\nFailed to create card file please check destination path";
        }
    }
}