package com.bdc.xmlbridge.facade;

import com.bdc.xmlbridge.model.request.ModifyAccountCreditLimitFileRequest;
import com.bdc.xmlbridge.service.LookupsMapperService;
import com.bdc.xmlbridge.utility.Files;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component
public class ModifyAccountCreditLimitMapper implements IModifyAccountCreditLimitMapper {
    @Resource
    private LookupsMapperService lookups;


    public String Mapping(ModifyAccountCreditLimitFileRequest modifyAccountCreditLimitFileRequest) {
        try {
            lookups.loadLookups();
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC001 :\nFailed to create contract file\nFailed to read lookups file\nPlease ask developer team to check looks dictionary path ";
        }
        this.ModifyAccountCreditLimitMapping(modifyAccountCreditLimitFileRequest, lookups);

        try {
            return Files.generateFile(modifyAccountCreditLimitFileRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return "BDC002 :\nFailed to create Contract file please check destination folder";
        }

    }
}