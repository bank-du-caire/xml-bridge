package com.bdc.xmlbridge.controller;

import com.bdc.xmlbridge.facade.CardMapper;
import com.bdc.xmlbridge.facade.ContractMapper;
import com.bdc.xmlbridge.facade.ModifyAccountCreditLimitMapper;
import com.bdc.xmlbridge.facade.SupplementaryMapper;
import com.bdc.xmlbridge.model.request.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class MapperController {

    @Resource
    private ContractMapper contractMapper;

    @Resource
    private CardMapper cardMapper;

    @Resource
    private SupplementaryMapper supplementaryMapper;

    @Resource
    private ModifyAccountCreditLimitMapper modifyAccountCreditLimitMapper;


    //  Contract File Creator webservice endpoint
    @PostMapping("/contractFile")
    public String createContractFile(@RequestBody IssuingContractIFileRequest issuingContractFileRequest) {
        System.out.println(issuingContractFileRequest);
        return contractMapper.Mapping(issuingContractFileRequest);
    }

    //  card File Creator webservice endpoint
    @PostMapping("/cardFile")
    public String createContractFile(@RequestBody IssuingCardFileRequest cardIssuingFileRequest) {
        System.out.println(cardIssuingFileRequest);
        return cardMapper.Mapping(cardIssuingFileRequest);
    }

    //  supplementary  File Creator webservice endpoint
    @PostMapping("/supplementary")
    public String createSupplementary(@RequestBody IssuingSupplementaryFileRequest supplementaryIssuingFileRequest) {
        System.out.println(supplementaryIssuingFileRequest);
        return supplementaryMapper.Mapping(supplementaryIssuingFileRequest);
    }

    //  Modify Customer_data File Creator webservice endpoint
    @PostMapping("/modify_account_credit_limit_mapper")
    public String modifyAccountCreditLimitMapper(@RequestBody ModifyAccountCreditLimitFileRequest modifyAccountCreditLimitFileRequest) {
        return modifyAccountCreditLimitMapper.Mapping(modifyAccountCreditLimitFileRequest);
    }
    //  Modify Customer_data File Creator webservice endpoint
    @PostMapping("/modify_customer_details_mapper")
    public String modifyCustomerDetailsMapper(@RequestBody ModifyAccountCreditLimitFileRequest modifyAccountCreditLimitFileRequest) {
        return modifyAccountCreditLimitMapper.Mapping( modifyAccountCreditLimitFileRequest);
    }
}


