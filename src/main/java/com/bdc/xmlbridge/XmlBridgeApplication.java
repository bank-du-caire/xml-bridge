package com.bdc.xmlbridge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlBridgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(XmlBridgeApplication.class, args);
    }

}
